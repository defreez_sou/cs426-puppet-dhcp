class dhcp
{
    package { 'dhcp':
        ensure  => installed,
    }

    service { 'dhcpd':
        ensure  => running,
        enable  => true,
        require => File['/etc/dhcp/dhcpd.conf'],
    }

    file { '/etc/dhcp/dhcpd.conf':
        owner   => root,
        group   => root,
        mode    => 0644,
        source  => 'puppet:///modules/dhcp/dhcpd.conf',
        require => Package['dhcp'],
        notify  => Service['dhcpd'],
    }
}
